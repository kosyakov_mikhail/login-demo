//
//  ViewController2.swift
//  Login app
//
//  Created by Macos on 13.05.2022.
//

import UIKit

class ViewController2: UIViewController {

// MARK: - IBOutlets
    
    @IBOutlet weak var vc2Label: UILabel!
    @IBOutlet weak var backButton: UIButton!
    
    var textLabel = ""
    var textButton = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        backButton.layer.cornerRadius = 10
        vc2Label.text = textLabel
        backButton.setTitle(textButton, for: .normal)
    }

    
    @IBAction func backButton(_ sender: Any) {
    }
    
    

 // MARK: - Deinit
    
    deinit {
        print("The VC2 has uploaded from memory")
    }
}

