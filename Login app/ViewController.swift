//
//  ViewController.swift
//  Login app
//
//  Created by Macos on 13.05.2022.
//

import UIKit

class ViewController: UIViewController {

// MARK: - IBOutlets
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    
// MARK: - Users
    
    var usersArray = [User(userName: "Egor", password: "Egor123"),
                      User(userName: "Mikhail", password: "Mikhail4321"),
                      User(userName: "Anna", password: "qwerty"),
                      User(userName: "Alexey", password: "Alex768")]

    
    class User {
        var userName: String
        var password: String

        init(userName: String, password: String) {
            self.userName = userName
            self.password = password
        }
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameTextField.delegate = self
        self.passwordTextField.delegate = self
        loginButton.layer.cornerRadius = 10
        
    }

// MARK: - IBActions
    
    @IBAction func loginButton(_ sender: Any) {
        let text = nameTextField.text!
        let text1 = passwordTextField.text!
        if usersArray.contains(where: { $0.userName == text }) &&
           usersArray.contains(where: {$0.password == text1 }) {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                if let vc2 = storyBoard.instantiateViewController(identifier: "VC2") as?
                    ViewController2 {
                    vc2.textLabel = "Здравствуй, \(nameTextField.text!)"
                    vc2.textButton = "Выйти"
                    show(vc2, sender: nil)
                }
                
            } else {
                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
                if let vc2 = storyBoard.instantiateViewController(identifier: "VC2") as?
                    ViewController2 {
                    vc2.textLabel = "Ошибка, неправильный логин или пароль"
                    vc2.textButton = "Попробовать снова"
                    show(vc2, sender: nil)
                }
                
            }
        }
    
    
// MARK: - unwinSegue
    
    @IBAction func unwindSegue(segue: UIStoryboardSegue) {
        nameTextField.text = nil
        passwordTextField.text = nil
    }
}


// MARK: - UITextFieldDelegate

extension ViewController: UITextFieldDelegate{
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        
        case nameTextField:
            nameTextField.resignFirstResponder()
            passwordTextField.becomeFirstResponder()
        break
        case passwordTextField:
            passwordTextField.resignFirstResponder()
        break
        default:
        break
            
        }
        
        return true
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        view.endEditing(true)
    }
}




